# Conversion of GOA gaf files to RDF

This projects aims at retrieving GOA files and converting them into RDF

# Files

- `goa2rdf.py` converts a gaf file into RDF
- `retrieveGeneOntologyAnnotations_ebi.bash` is a script for downloading a gaf file from the EBI repository, converting it into RDF and setting up a TDB data respository, e.g. for fuseki.

# RDF data structure

![Data structure of GOA in RDF](./goa.png)

# Todo

- [ ] add the proper metadata related to the generated RDF files
    - [ ] PROV
    - [ ] FAIR
- [ ] generate a named graph

