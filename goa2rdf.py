#! /usr/bin/env python

import sys

indexDB = 0
indexUniprot = 1
indexProteinName = 2
indexModifier = 3
indexGO = 4
indexDBref = 5
indexEvidence = 6
indexBranch = 8
indexTaxon = 12

evidence = {}
evidence["HDA"] = "eco:0007005"
evidence["IBA"] = "eco:0000318"
evidence["IC"] = "eco:0000001"
evidence["IDA"] = "eco:0000314"
evidence["IEA"] = "eco:0000501"
evidence["IEP"] = "eco:0000008"
evidence["IGC"] = "eco:0000317"
evidence["IGI"] = "eco:0000316"
evidence["IMP"] = "eco:0000315"
evidence["IPI"] = "eco:0000353"
evidence["ISA"] = "eco:0000247"
evidence["ISM"] = "eco:0000202"
evidence["ISS"] = "eco:0000044"
evidence["NAS"] = "eco:0000303"
evidence["ND"] = "eco:0000035"
evidence["RCA"] = "eco:0000245"
evidence["TAS"] = "eco:0000304"
evidence["EXP"] = "eco:0000006"
evidence["IKR"] = "eco:0000320"
evidence["IMR"] = "eco:0000320"
evidence["IRD"] = "eco:0000321"
evidence["HEP"] = "eco:0007007"
evidence["HMP"] = "eco:0007001"
evidence["ISO"] = "eco:0000266"
#evidence[""] = "eco:0000"

goarelation = {}
goarelation["P"] = "goavoc:process"
goarelation["NOTP"] = "goavoc:not-in-process"
goarelation["C"] = "goavoc:component"
goarelation["NOTC"] = "goavoc:not-in-component"
goarelation["F"] = "goavoc:function"
goarelation["NOTF"] = "goavoc:not-has-function"

i = 0
knownTaxon = []
with open(sys.argv[1]) as dataFile:
    with open(sys.argv[1] + ".ttl", "w") as rdfFile:
        rdfFile.write("""
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix go: <http://purl.obolibrary.org/obo/GO_> .
#@prefix go: <http://bio2rdf.org/go:> .
@prefix uniprot: <http://purl.uniprot.org/uniprot/> .
@prefix uniprotCore: <http://purl.uniprot.org/core/> .
@prefix pubmed: <http://www.ncbi.nlm.nih.gov/pubmed/> .
#@prefix pubmed: <http://bio2rdf.org/pubmed:> .
@prefix eco: <http://purl.obolibrary.org/obo/ECO_> .
#@prefix eco: <http://bio2rdf.org/eco:> .
@prefix goavoc: <http://bio2rdf.org/goa_vocabulary:> .
@prefix annot: <http://www.irisa.fr/dyliss/public/odameron/annotation/goa_> .
@prefix taxon: <http://purl.bioontology.org/ontology/NCBITAXON/> .
@prefix taxonUniprot: <http://purl.uniprot.org/taxonomy/> .

""")
        for (evidenceName,evidenceID) in evidence.items():
            rdfFile.write(evidenceID + " rdfs:label \"" + evidenceName + "\" .\n")
        rdfFile.write("\n")
        indiceAnnotation = 0
        currentLine = dataFile.readline()
        while(currentLine != ""):
            #print(currentLine)
            currentLine = currentLine.replace("\n","")
            if (currentLine == "") or (currentLine[0] == "!"):
                currentLine = dataFile.readline()
                continue
            indiceAnnotation += 1
            i += 1
            # if i > 3:
            #     break
            values = currentLine.split("\t")
            if values[indexDB] != "UniProtKB":
                currentLine = dataFile.readline()
                continue
            print("")
            print(str(values))
            print("UniprotID: " + values[indexUniprot])
            print("Protein name: " + values[indexProteinName])
            print("Qualifier: " + values[indexModifier])
            print("GO: " + values[indexGO])
            print("Ref: " + values[indexDBref])
            print("Evidence: " + values[indexEvidence])
            print("Taxon: " + values[indexTaxon])
            if values[indexModifier] != "NOT":
                values[indexModifier] = ""
            if "|" in values[indexTaxon]:
                values[indexTaxon] = values[indexTaxon].split("|")[0]
            taxonID = values[indexTaxon].replace("taxon:", "")
            annotationID = taxonID + "-" + str(indiceAnnotation)
            rdfFile.write("\n")
            rdfFile.write("uniprot:" + values[indexUniprot] + " rdf:type uniprotCore:Protein .\n")
            rdfFile.write("uniprot:" + values[indexUniprot] + " rdfs:label \"" + values[indexProteinName] + "\" .\n")
            rdfFile.write("uniprot:" + values[indexUniprot] + " goavoc:go-annotation annot:" + annotationID + " .\n")
            rdfFile.write("uniprot:" + values[indexUniprot] + " uniprotCore:organism taxon:" + taxonID + " .\n")
            rdfFile.write("uniprot:" + values[indexUniprot] + " uniprotCore:organism taxonUniprot:" + taxonID + " .\n")
            if taxonID not in knownTaxon:
                knownTaxon.append(taxonID)
                rdfFile.write("taxonUniprot:" + taxonID + " rdf:type uniprotCore:Taxon .\n")
            rdfFile.write("annot:" + annotationID + " rdf:type goavoc:GO-Annotation .\n")
            rdfFile.write("annot:" + annotationID + " goavoc:go-term  " + values[indexGO].replace("GO:", "go:") + " .\n")
            rdfFile.write("annot:" + annotationID + " goavoc:evidence " + evidence[values[indexEvidence]] + " .\n")
            if values[indexDBref].startswith("PMID:"):
                rdfFile.write("annot:" + annotationID + " goavoc:article pubmed:" + values[indexDBref].replace("PMID:","") + " .\n")
            rdfFile.write("uniprot:" + values[indexUniprot] + " " + goarelation[values[indexModifier] + values[indexBranch]] + " " + values[indexGO].replace("GO:", "go:") + " .\n")

            currentLine = dataFile.readline()
