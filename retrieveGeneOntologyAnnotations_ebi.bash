#! /usr/bin/env bash

species=human
#species=arabidopsis
#species=mouse

jenaroot=/usr/local/semanticWeb/jena
goroot=~/ontology/geneOntology
fusekiroot=/usr/local/semanticWeb/fuseki

goDir=~/ontology/geneOntology
speciesDir=${goDir}/${species}
tdbDir=${goDir}/tdb-${species}-goUniprotUri


# create directories
if [ -d ${speciesDir} ]
then
  rm -rf ${speciesDir}/*
else
  mkdir ${speciesDir}
fi

if [ -d ${tdbDir} ]
then
  rm -rf ${tdbDir}/*
else
  mkdir ${tdbDir}
fi


# download goa file
#wget http://download.bio2rdf.org/release/3/goa/goa_${species}.nq.gz
#wget --directory-prefix ${speciesDir} ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/HUMAN/gene_association.goa_${species}.gz
#wget --directory-prefix ${speciesDir} ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/${species^^}/gene_association.goa_${species}.gz
#gunzip ${speciesDir}/gene_association.goa_${species}.gz
wget --directory-prefix ${speciesDir} ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/${species^^}/goa_${species}.gaf.gz
gunzip ${speciesDir}/goa_${species}.gaf.gz


# convert to rdf
python3 ./goa2rdf.py ${speciesDir}/goa_${species}.gaf
 

# generate TBD
#${jenaroot}/bin/tdbloader2 --loc=${tdbDir}/ ${speciesDir}/goa_${species}.gaf.ttl
#${jenaroot}/bin/tdbloader --loc=${tdbDir}/ ${goDir}/go-latest.owl
#${jenaroot}/bin/tdbloader --loc=${tdbDir}/ ${goDir}/evidenceCode.owl
${JENA_HOME}/bin/tdb1.xloader --loc ${tdbDir} ${speciesDir}/goa_${species}.gaf.ttl ${goDir}/go-latest.owl ${goDir}/evidenceCode.owl

# run fuseki
#${fusekiroot}/fuseki-server --update --loc ${tdbPath}/ /goa
echo "Run fuseki:"
#echo "${fusekiroot}/fuseki-server --update --loc ${tdbDir}/ /goa"
echo "${fusekiroot}/fuseki-server --loc ${tdbDir}/ /goa"
